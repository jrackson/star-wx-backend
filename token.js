const _ = require('lodash')
const user = require('./db/models/user')
const tool = require('./utils/tool')
// const debug = require('debugger')('token')

// 不需要验证的url
let igUrls = ['/api','/ruzhu','/weapp']

const token = async (ctx, next) => {
    let url = ctx.url;
    let result = _.find(igUrls, (item) => {
        let regExp = new RegExp('^' + item, 'i');
        return regExp.test(url);
    })
    if (result) {
        await next()
    } else {
        let token = ctx.cookies.get('token')
        if(token){
            token = JSON.parse(token)
        } else {
            throw '没有令牌'
        }
        ctx.state.username = token.username
        let one = await user.findByNameAndToken(token.username, tool.md5(token.value))
        if (one) {
            if (one.token.expire.getTime() > new Date().getTime())
                await next()
            else
                throw '令牌授权过期'
        } else {
            throw '无效令牌'
        }
    }
}

module.exports = token