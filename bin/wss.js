const WebSocket = require('ws');
let wss;

const createWss = function(server) {
    wss = new WebSocket.Server({ server });
    //广播
    wss.broadcast = function(message) {
        wss.clients.forEach(function(client) {
            client.send(message);
        });
    };

    wss.on('connection', function connection(ws, req) {

        ws.on('message', function(message) {
            // var data = JSON.parse(message)
            console.log(message);
            wss.broadcast(message)
        });

        ws.on('close', function() {
            console.log('_close');
        })

        ws.on('error', function() {
            console.log('_error')
        })

        console.log('在线人数:' + wss.clients.size)
    });
}

const broadcast = function(message) {
    wss && wss.broadcast(message)
}

module.exports = {
    createWss:createWss,
    broadcast:broadcast
}