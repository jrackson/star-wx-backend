const _ = require('lodash')
/**
 * 基础信息类
 */
class ReportList {
    /**
     * 构造函数
     */
    constructor() {
        this.list = []
    }
    /**
     * 添加接口信息
     * @param {Array} infos 
     */
    addReport(report) {
        let result = _.find(this.list, (item) => {
            return item.openid == report.openid
        })
        if (!result)
            this.list.push(report)
    }

    async sendMsg() {
        
        for (let item of this.list) {
            let url = 'http://kefu.pkfis.cn/message/report'
            await tool.httpPost(url, item);
        }
        this.list = []
    }
}

module.exports = new ReportList()