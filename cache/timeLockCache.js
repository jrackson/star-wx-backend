/**
 * 时间同步
 */
const moment = require('moment');
const webConfig = require('../config/web_config');
const logUitl = require('../utils/log_util')
const tool = require('../utils/tool')

class TimeLock {
    constructor() {
        this.difTime = 0;
        this.timer = undefined;
    }

    /**
     * 初始化服务器时间
     */
    initServerTime() {
        if (!webConfig.getSystemDateUrl) return;
        // 计算一次时间差
        this.calcDif();
        // 每一小时计算一次时间差
        this.timer = setInterval(this.calcDif, 60 * 60 * 1000)
    }

    /**
     * 计算时间差
     */
    async calcDif() {
        try {
            let req = await tool.httpGet(webConfig.getSystemDateUrl, {});
            let body = req.body;
            if (body.result) {
                let serverTime = moment(body.data);
                this.difTime = serverTime.diff(moment());
                logUitl.logInfo('时间同步成功当前时间：' + this.nowTime.format('YYYY-MM-DD HH:mm:ss'))
            } else {
                logUitl.logInfo(body.error)
            }
        } catch (err) {
            logUitl.logInfo('时间同步请求异常')
        }
    }

    /**
     * 获取当前时间
     */
    get nowTime() {
        return moment().add(this.difTime, 'milliseconds')
    }

    /**
     * 关闭同步
     */
    clearTimer() {
        this.timer && clearInterval(this.timer);
    }
}


module.exports = new TimeLock()