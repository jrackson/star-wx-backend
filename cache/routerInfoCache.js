/**
 * 基础信息类
 */
class RouterInfo {
    /**
     * 构造函数
     */
    constructor() {
        this.routerInfo = []
    }
    /**
     * 添加接口信息
     * @param {Array} infos 
     */
    addInfo(infos) {
        if (Array.isArray(infos))
            this.routerInfo = this.routerInfo.concat(infos)
        else
            this.routerInfo.push(infos)
    }
}

module.exports = new RouterInfo()