const webConfig = require('../config/web_config')
const logUtil = require('../utils/log_util')
const tool = require('../utils/tool');

module.exports = connection = {}

/**
 * 连接状态
 */
connection.connectionState = {}

/**
 * 特定请求判断连接状态 返回状态
 * @param {string} name 需要请求的服务名字 根据配置文件获取
 */
connection.apiConnect = async function (name) {
    const urls = webConfig.connUrls;
    let url;
    let text;
    for (var i in urls) {
        if (urls[i].name == name) {
            url = urls[i].url;
            text = urls[i].text;
        }
    }
    try {
        if (url) {
            await tool.httpGet(url, {});
            this.connectionState[name] = true;
            return {
                result: 1,
                data: text + '--接口网络通讯正常'
            }
        } else {
            return {
                result: 0,
                data: text + '--没有找到对应接口名称'
            }
        }
    } catch (e) {
        this.connectionState[name] = false;
        return {
            result: 0,
            data: text + '--接口网络通讯异常'
        }
    }
}

/**
 * 初始化连接 并将状态保存到 connectionState
 */
connection.initConnection = async function () {
    const urls = webConfig.connUrls;
    for (var i in urls) {
        let key = urls[i].name
        try {
            await tool.httpGet(urls[i].url, {});
            this.connectionState[key] = true;
            logUtil.logInfo(urls[i].text + '--网络正常');
        } catch (e) {
            this.connectionState[key] = false;
            logUtil.logInfo(urls[i].text + '--网络异常');
        }
    }
}