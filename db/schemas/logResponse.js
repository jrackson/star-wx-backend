const mongoose = require('mongoose');

var Schema = mongoose.Schema({
    reqOriginalUrl: String,
    reqMethod: String,
    reqClientIP: String,
    reqQuery: Object,
    reqBody: Object,
    resbody: Object,
    resUseTime: Number,
    resStatus: String,
    meta: { 
        createAt: { //创建时间
            type: Date, //类型
            default: Date.now() //默认值
        },
        updateAt: { //更新时间
            type: Date, //类型
            default: Date.now() //默认值
        }
    }
})

Schema.pre('save', function(next) {
    if (this.isNew) {
        this.meta.createAt = this.meta.updateAt = Date.now();
    } else {
        this.meta.updateAt = Date.now()
    }
    next()
})

module.exports = Schema;