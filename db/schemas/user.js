const mongoose = require('mongoose');

var Schema = mongoose.Schema({
    name: String,
    secret: String,
    token: {
        value: String,
        expire: Date
    },
    meta: {
        createAt: { //创建时间
            type: Date, //类型
            default: Date.now() //默认值
        },
        updateAt: { //更新时间
            type: Date, //类型
            default: Date.now() //默认值
        }
    }
})

Schema.pre('save', function (next) {
    if (this.isNew) {
        this.meta.createAt = this.meta.updateAt = Date.now();
    } else {
        this.meta.updateAt = Date.now()
    }
    next()
})
/**
 * 通过名字搜索
 * @param {*} name 
 * @param {*} cb 
 */
Schema.statics.findByName = function (name, cb) {
    return this.findOne({
        name: name
    }, cb);
}
/**
 * 查找名字和token对应
 * @param {*} name 
 * @param {*} token 
 * @param {*} cb 
 */
Schema.statics.findByNameAndToken = function (name, token, cb) {
    return this.findOne({
        name: name,
        "token.value": token
    }, cb);
}

module.exports = Schema;