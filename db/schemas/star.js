const mongoose = require('mongoose');

var Schema = mongoose.Schema({
    type: {
        type: String,
        default: '亚太'
    },
    name: {
        type: String,
        default: ''
    },
    userimg: {
        type: String,
        default: ''
    },
    poster: {
        type: String,
        default: ''
    },
    words: [{
        openid: String,
        nickName: String,
        avatarUrl: String,
        value: String
    }],
    hitFans: [{
        openid: String,
        nickName: String,
        avatarUrl: String,
        hitNumb: Number,
        allHitNumb: Number
    }],
    hitFansOrderAll: [{
        openid: String,
        nickName: String,
        avatarUrl: String,
        hitNumb: Number,
        allHitNumb: Number
    }],
    sort: Number,
    news: [],
    weekHot: {
        type: Number,
        default: 0
    },
    lastWeekHot: {
        type: Number,
        default: 0
    },
    hot: {
        type: Object,
        default: {}
    },
    touping:{   //投屏值应援
        type:Number,
        default:0
    },
    biaoqing:{  //表情包应援
        type:Number,
        default:0
    },
    shengri:{   //生日应援
        type:Number,
        default:0
    },
    meta: {
        createAt: { //创建时间
            type: Date, //类型
            default: Date.now() //默认值
        },
        updateAt: { //更新时间
            type: Date, //类型
            default: Date.now() //默认值
        }
    },
    sort: Number
})

Schema.pre('save', function (next) {
    if (this.isNew) {
        this.meta.createAt = this.meta.updateAt = Date.now();
    } else {
        this.meta.updateAt = Date.now()
    }
    next()
})
/**
 * 通过条件搜索分页
 * @param {*} name 
 * @param {*} cb 
 */
Schema.statics.findByCondition = function (condition, pagination, out, cb) {
    out = out || {}
    let skip = (pagination.page - 1) * pagination.rows
    let sort = {}
    sort[pagination.sortname] = pagination.sortorder
    if (pagination.pagination)
        return this.find(condition, out).sort(sort).skip(skip).limit(parseInt(pagination.rows)).exec(cb);
    else
        return this.find(condition, out).sort(sort).exec(cb);
}
/**
 * 查找名字和token对应
 * @param {*} name 
 * @param {*} token 
 * @param {*} cb 
 */
Schema.statics.countByCondition = function (condition, cb) {
    return this.countDocuments(condition).exec(cb);
}

module.exports = Schema;