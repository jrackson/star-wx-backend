const mongoose = require('mongoose');

var Schema = mongoose.Schema({
    name: String,
    imgUrl: String,
    toUrl: String,
    meta: {
        createAt: { //创建时间
            type: Date, //类型
            default: Date.now() //默认值
        },
        updateAt: { //更新时间
            type: Date, //类型
            default: Date.now() //默认值
        }
    }
})

Schema.pre('save', function (next) {
    if (this.isNew) {
        this.meta.createAt = this.meta.updateAt = Date.now();
    } else {
        this.meta.updateAt = Date.now()
    }
    next()
})
/**
* 通过条件搜索分页
* @param {*} name 
* @param {*} cb 
*/
Schema.statics.findByCondition = function (condition, pagination, out, cb) {
   out = out || {}
   let skip = (pagination.page - 1) * pagination.rows
   let sort = {}
   sort[pagination.sortname] = pagination.sortorder
   if (pagination.pagination)
       return this.find(condition, out).sort(sort).skip(skip).limit(parseInt(pagination.rows)).exec(cb);
   else
       return this.find(condition, out).sort(sort).exec(cb);
}
/**
* 查找名字和token对应
* @param {*} name 
* @param {*} token 
* @param {*} cb 
*/
Schema.statics.countByCondition = function (condition, cb) {
   return this.countDocuments(condition).exec(cb);
}

module.exports = Schema;