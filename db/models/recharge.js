const mongoose = require('mongoose')
const schema = require('../schemas/recharge')

module.exports = mongoose.model('recharge', schema);
