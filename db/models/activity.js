const mongoose = require('mongoose')
const schema = require('../schemas/activity')

module.exports = mongoose.model('activity', schema);
