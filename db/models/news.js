const mongoose = require('mongoose')
const schema = require('../schemas/news')

module.exports = mongoose.model('news', schema);
