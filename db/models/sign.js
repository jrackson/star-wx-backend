const mongoose = require('mongoose')
const schema = require('../schemas/sign')

module.exports = mongoose.model('sign', schema);
