const mongoose = require('mongoose')
const schema = require('../schemas/rStarAndWxUser')

module.exports = mongoose.model('rStarAndWxUser', schema);
