const mongoose = require('mongoose')
const schema = require('../schemas/wxUser')

let model = mongoose.model('wxUser', schema);

module.exports = model