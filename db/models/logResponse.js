const mongoose = require('mongoose')
const schema = require('../schemas/logResponse')

module.exports = mongoose.model('logResponse', schema);