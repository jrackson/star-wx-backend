const mongoose = require('mongoose')
const schema = require('../schemas/type')

module.exports = mongoose.model('type', schema);
