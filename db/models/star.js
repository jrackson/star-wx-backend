const mongoose = require('mongoose')
const schema = require('../schemas/star')

module.exports = mongoose.model('star', schema);

