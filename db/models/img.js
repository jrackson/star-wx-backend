const mongoose = require('mongoose')
const schema = require('../schemas/img')

module.exports = mongoose.model('img', schema);
