const mongoose = require('mongoose');
const webConfig = require('../config/web_config');
const logUtil = require('../utils/log_util')

const options = {
    autoIndex: false, // Don't build indexes
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
    bufferCommands: false
};

const initDB = async function () {
    try {
        if (webConfig.mongodbConnection) {
            var db = await mongoose.connect(webConfig.mongodbConnection, {
                useNewUrlParser: true
            })
            if (db.connection.readyState) {
                logUtil.logInfo('提示：数据库连接成功')
                db.connection.on('connected', () => {
                    logUtil.logInfo('提示：数据库已连接,连接数为' + db.connections.length)
                })
                db.connection.on('disconnected', (a) => {
                    logUtil.logInfo('提示：数据库断开连接!!!,连接数为' + db.connections.length)
                })
            } else {
                logUtil.logInfo('错误：数据库连接失败')
            }
        } else {
            logUtil.logInfo('警告：没有配置数据库连接')
        }
    } catch (err) {
        logUtil.logInfo('错误：数据库连接失败-'+err.message)
    }
}

module.exports = {
    initDB: initDB
}