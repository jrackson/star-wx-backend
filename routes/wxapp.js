const router = require('koa-router')()
const ctrl = require('../wxControllers/user'); //用户模块逻辑层
// let routerInfo = require('../cache/routerInfoCache')

router.prefix('/weapp')

router.post('/login', ctrl.login) //验证用户名密码
      .post('/userinfo', ctrl.userinfo) //获取所有用户
      .post('/share', ctrl.share) //获取所有用户
    //   .post('/newUser', ctrl.newUser) //获取所有用户
      
module.exports = router

