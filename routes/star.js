const router = require('koa-router')()

const starCtrl = require('../controller/StarController')

router.prefix('/star')

router.post('/', starCtrl.find) //首页
  .post('/insert', starCtrl.insert) //获取是否连接
  .post('/update', starCtrl.update) //获取服务器当前时刻
  .post('/delete', starCtrl.delete) //验证用户名密码

module.exports = router