const router = require('koa-router')()
const routerInfo = require('../cache/routerInfoCache')
const userctrl = require('../controller/UserController')
const indexctrl = require('../controller/IndexController')

const starctrl = require('../controller/StarController')
const wxUserctrl = require('../controller/WxUserController')
const imgctrl = require('../controller/ImgController')
const typectrl = require('../controller/TypeController')
const newsctrl = require('../controller/NewsController')
const signctrl = require('../controller/SignController')
const activityctrl = require('../controller/ActivityController')
const taskctrl = require('../controller/taskConrtoller')

const rStarAndWxCtrl = require('../controller/rStarAndWxUserController')
router.prefix('/api')

router.get('/', indexctrl.api) //首页
  .get('/conn', indexctrl.conn) //获取是否连接
  .get('/time', indexctrl.time) //获取服务器当前时刻
  .post('/downloadUrl', indexctrl.downloadUrl) //获取服务器当前时刻
  .post('/endWeek', indexctrl.endWeek) //获取服务器当前时刻

  .post('/validate', userctrl.validate) //验证用户名密码
  .post('/token', userctrl.token) //判断token是否有效

  .post('/findStarByName', starctrl.findStarByName)
  .post('/findStarSort', starctrl.findStarSort)
  .post('/findStarById', starctrl.findStarById)
  .post('/findKuaibao', starctrl.findKuaibao)
  .post('/findHotById', starctrl.findHotById)
  .post('/findHotByIds', starctrl.findHotByIds)
  .post('/sendWord', starctrl.sendWord)
  .post('/hitFans', starctrl.hitFans)
  .post('/sort', starctrl.sort)

  .post('/findImgs', imgctrl.findImgs)

  .post('/findTypes', typectrl.findTypes)

  .post('/findUserById', wxUserctrl.findUserById)

  .post('/findOpenidByUid', wxUserctrl.findUidByOpenid)

  .post('/getNews', newsctrl.getNews)
  .post('/getNewsById', newsctrl.getNewsById)

  .post('/sign', signctrl.sign)
  .post('/shouhu', signctrl.shouhu)
  .post('/activity',activityctrl.activity)
  .post('/getTask',taskctrl.getTask)

  .post('/report', signctrl.report)

  .post('/sendMessage', async (ctx, next) => {
    require('../cache/reportListCache').sendMsg();
  })

  .post('/fansSort', rStarAndWxCtrl.find)
  .get('/everyWeek', rStarAndWxCtrl.everyWeek)

// 添加接口信息
routerInfo.addInfo([{
  url: '/conn/',
  args: [],
  introduction: '获取是否连接和当前服务与其他服务的连接状态'
}, {
  url: '/api/time',
  args: [],
  introduction: '获取服务器当前时刻'
}, {
  url: '/api/validate',
  args: [{
    name: 'username',
    text: '用户名',
    type: 'string',
    must: true
  }, {
    name: 'secret',
    text: '密钥',
    type: 'string',
    must: true
  }],
  introduction: '验证用户名密码'
}, {
  url: '/api/token',
  args: [{
    name: 'username',
    text: '用户名',
    type: 'string',
    must: true
  }, {
    name: 'token',
    text: '令牌',
    type: 'string',
    must: true
  }],
  introduction: '判断token是否有效'
}, {
  url: '/api/findStarByName',
  args: [{
    name: 'name',
    text: '明星团名字',
    type: 'string',
    must: true
  }],
  introduction: '模糊搜索明星团'
}, {
  url: '/api/findStarSort',
  args: [{
    name: 'date',
    text: '日期（例如:201808，默认：本月）',
    type: 'string',
    must: false
  }],
  introduction: '明星团热门排名接口'
}, {
  url: '/api/findStarById',
  args: [{
    name: 'id',
    text: '明星团ID',
    type: 'string',
    must: true
  }, {
    name: 'n',
    text: '粉丝排行显示数目（默认：5）',
    type: 'number',
    must: false
  }],
  introduction: '明星团详情接口'
}, {
  url: '/api/findKuaibao',
  args: [],
  introduction: '快报接口（默认10条记录）'
}, {
  url: '/api/findHotById',
  args: [{
    name: 'id',
    text: '明星团ID',
    type: 'string',
    must: true
  }],
  introduction: '获取明星团热度'
}, {
  url: '/api/sendWord',
  args: [{
    name: 'starid',
    text: '明星团id',
    type: 'string',
    must: true
  }, {
    name: 'openid',
    text: '微信用户id',
    type: 'string',
    must: true
  }, {
    name: 'value',
    text: '弹幕内容',
    type: 'string',
    must: true
  }],
  introduction: '发表弹幕接口'
}, {
  url: '/api/hitFans',
  args: [{
    name: 'starid',
    text: '明星团id',
    type: 'string',
    must: true
  }, {
    name: 'openid',
    text: '微信用户id',
    type: 'string',
    must: true
  }, {
    name: 'count',
    text: '积分数目',
    type: 'number',
    must: true
  }],
  introduction: '粉丝打榜接口'
}, {
  url: '/api/findImgs',
  args: [],
  introduction: '获取轮播图'
}, {
  url: '/api/findUserById',
  args: [{
    name: 'openid',
    text: '微信用户id',
    type: 'string',
    must: true
  }, {
    name: 'n',
    text: '显示打榜明星数量 （默认：5）',
    type: 'number',
    must: false
  }],
  introduction: '获取用户详情'
},{
  url:'/api/activity',
  args:[{
    name:'unionId',
    text:'跨平台用户唯一标识',
    type:'string',
    must:true
  }, {
    name:'name',
    text:'应援明星姓名',
    type:'string',
    must:true
  },{
    name:'type',
    text:'应援类型',
    type:'string',
    must:true
  }
  ],
  introduction:'用户参与公众号应援活动'
},{
  url:'/api/getTask',
  args:[{
    name: 'openid',
    text: '微信用户id',
    type: 'string',
    must: true
  },{
    name: 'starid',
    text: '明星团id',
    type: 'string',
    must: true
  }],
  introduction:'获取用户任务信息'
}])

module.exports = router