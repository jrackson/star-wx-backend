const router = require('koa-router')()

const ctrl = require('../controller/NewsController')

router.prefix('/news')

router.post('/', ctrl.find) //首页
  .post('/insert', ctrl.insert) //获取是否连接
  .post('/update', ctrl.update) //获取服务器当前时刻
  .post('/delete', ctrl.delete) //验证用户名密码

module.exports = router