const router = require('koa-router')()

const wxUserCtrl = require('../controller/WxUserController')

router.prefix('/wxUser')

router.post('/', wxUserCtrl.find) //首页
  .post('/insert', wxUserCtrl.insert) //获取是否连接
  .post('/update', wxUserCtrl.update) //获取服务器当前时刻
  .post('/delete', wxUserCtrl.delete) //验证用户名密码

module.exports = router