const router = require('koa-router')()
const userctrl = require('../controller/UserController'); //用户模块逻辑层
let routerInfo = require('../cache/routerInfoCache')

router.prefix('/user')

router.post('/validate', userctrl.validate) //验证用户名密码
      .post('/token', userctrl.token) //获取所有用户
      .post('/newUser', userctrl.newUser) //获取所有用户
      .post('/changePwd', userctrl.changePwd) 
      
module.exports = router