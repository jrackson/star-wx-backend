/**
 * 工具模块
 */
module.exports = tool = {};

const agent = require("superagent");
const crypto = require('crypto');
const _ = require('lodash')

/**
 * POST请求
 * @param {string} url url地址
 * @param {object} options 请求参数
 * @param {object} headers 请求头参数
 */
tool.httpPost = function (url, options, headers = {}) {
    return agent
        .post(url)
        .send(options)
        .set(headers)
}

/**
 * GET请求
 * @param {string} url url地址
 * @param {object} options url参数
 * @param {object} headers 请求头参数
 */
tool.httpGet = function (url, options, headers = {}) {
    return agent
        .get(url)
        .query(options)
        .set(headers)
}

/**
 * 将object的key值转小写  
 * @param {object} obj 对象
 */
tool.lowerObjKey = function (obj) {
    let newObj = _.mapKeys(obj, function (value, key) {
        return key.toLowerCase();
    });
    return newObj;
}
/**
 * 返回成功
 * @param {*} ctx 上下文
 * @param {*} data 返回信息
 */
tool.resSuccess = function (ctx, data) {
    ctx.body = {
        result: 1,
        data: data
    }
}
/**
 * 返回失败
 * @param {*} ctx 上下文
 * @param {*} data 返回信息
 * @param {*} error 返回错误
 */
tool.resError = function (ctx, data, error = null) {
    ctx.body = {
        result: 0,
        data: data,
        error: error.message || error
    }
}
tool.md5 = function (value) {
    var md5 = crypto.createHash('md5');
    var result = md5.update(value).digest('hex');
    return result
}

tool.isHas = (args, attrs) => {
    attrs = attrs instanceof Array ? attrs : [attrs] 
    _.forEach(attrs, (item) => {
        if (!_.has(args, item)) {
            throw `缺少参数${item}`
        }
    })
}

tool.pageModel = {
    pagination: true,
    page: 1,
    rows: 10,
    sortname: 'meta.updateAt',
    sortorder: -1
}

tool.toRegExp = (object, attrs) => {
    attrs = attrs instanceof Array ? attrs : [attrs] 
    _.forEach(attrs, (item) => {
        object[item] = new RegExp(object[item])
    })
}