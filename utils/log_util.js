/**
 * 日志管理 用于pm2日志管理
 */
module.exports = logUtil = {};
/**
 * 封装错误日志
 * @param {ctx} ctx 上下文 
 * @param {error} error 错误信息
 * @param {number} resTime 耗时
 */
logUtil.logError = function (ctx, error, resTime) {
    if (ctx && error) {
        console.error(formatErrorJson(ctx, error, resTime)+'\n\n')
    }
};

/**
 * 封装响应日志
 * @param {ctx} ctx 上下文
 * @param {number} resTime 耗时
 */
logUtil.logResponse = function (ctx, resTime) {
    if (ctx) {
        // console.log(formatResJson(ctx, resTime)+'\n\n');
    }
};

/**
 * 封装记录日志
 * @param {string} info 信息
 */
logUtil.logInfo = function (info) {
    if (info) {
        // console.log(formatInfoJson(info)+'\n\n');
    }
};

/**
 * 格式化响应日志 to JSON
 * @param {ctx} ctx 上下文
 * @param {number} resTime 耗时
 */
var formatResJson = function (ctx, resTime) {
    let logText = {
        request: formatReqLogJson(ctx.request, resTime),
        status: ctx.status,
        resBody: ctx.body
    }
    return JSON.stringify(logText);
}

/**
 * 格式化错误日志 to JSON
 * @param {ctx} ctx 上下文
 * @param {error} error 错误信息
 * @param {*} resTime 耗时
 */
var formatErrorJson = function (ctx, error, resTime) {
    let logText = {
        request: formatReqLogJson(ctx.request, resTime),
        errName: error.name,
        errMessage: error.message,
        errStack: error.stack
    }
    return JSON.stringify(logText);
};

/**
 * 格式化记录日志 to JSON
 * @param {string} info 信息
 */
var formatInfoJson = function (info) {
    let logText = {
        message: info
    }
    return JSON.stringify(logText);
}

/**
 * 格式化请求体
 * @param {request} req 请求体
 * @param {number} resTime 耗时
 */
var formatReqLogJson = function (req, resTime) {
    let logText = {
        method: req.method,
        originalUrl: req.originalUrl,
        ip: req.ip,
        body: req.method == "GET" ? req.query : req.body,
        resTime: resTime
    }
    return logText;
}