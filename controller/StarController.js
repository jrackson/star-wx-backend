const model = require('../db/models/star')
const wxUser = require('../db/models/wxUser')
const recharge = require('../db/models/recharge')
const rStarAndWxUser = require('./rStarAndWxUserController')
const tool = require('../utils/tool')
const logUtil = require('../utils/log_util')
const _ = require('lodash')
const moment = require('moment')

async function methodFindHot(id, obj = {}) {
    if (!id) throw '缺少id参数'
    var result = await model.find({
        _id: id
    }, {
        hot: 1,
        sort: 1,
        weekHot: 1,
        lastWeekHot: 1
    })
    if (!result.length) throw '未找到对应记录'


    let date;
    if(obj.date){
        date = moment(obj.date).format('YYYYMM')
    }else{
        date = moment().format('YYYYMM')
    }
    let one = _.clone(result[0])
    if (obj.week === 0) {
        one.hot = one.weekHot
    } else if (obj.week === -1) {
        one.hot = one.lastWeekHot
    } else {
        one.hot = one.hot[date] || 0
    }
    return one
}

async function methodFindHots(ids, obj = {}) {
    if (!ids) throw '缺少id参数'
    var result = await model.find({
        $or: ids
    }, {
        hot: 1,
        sort: 1,
        weekHot: 1,
        lastWeekHot: 1
    })
    if (!result.length) throw '未找到对应记录'


    let date;
    if(obj.date){
        date = moment(obj.date).format('YYYYMM')
    }else{
        date = moment().format('YYYYMM')
    }
    let list = _.clone(result)
    if (obj.week === 0) {
        list = list.map(v=>{
            return Object.assign({},{_id:v._id,hot:v.weekHot,sort:v.sort})
        })
    } else if (obj.week === -1) {
        list = list.map(v=>{
            return Object.assign({},{_id:v._id,hot:v.lastWeekHot,sort:v.sort})
        })
    } else {
        list = list.map(v=>{
            return Object.assign({},{_id:v._id,hot:v.hot[date] || 0,sort:v.sort})
        })
    }
    return list
}
class Controller {
    static async sort(ctx) {
        try {
            let date = moment().format('YYYYMM').toString()
            let pagination = _.cloneDeep(tool.pageModel)
            pagination.sortname = `hot.${date}`
            pagination.pagination = false
            let rows = await model.findByCondition({}, pagination)
            _.forEach(rows, async (row, i) => {
                row.sort = i + 1
                await row.save()
            })
            if (ctx)
                tool.resSuccess(ctx, '成功')
        } catch (err) {
            if (ctx)
                tool.resError(ctx, '查找明星失败', err)
            logUtil.logInfo('查找明星失败' + err);
        }
    }
    static async weekDo(ctx) {
        try {
            let date = moment().format('YYYYMM').toString()
            let pagination = _.cloneDeep(tool.pageModel)
            pagination.sortname = `hot.${date}`
            pagination.pagination = false
            let rows = await model.findByCondition({}, pagination)
            _.forEach(rows, async (row, i) => {
                row.lastWeekHot = row.weekHot
                row.weekHot = 0
                await row.save()
            })
            if (ctx)
                tool.resSuccess(ctx, '成功')
        } catch (err) {
            if (ctx)
                tool.resError(ctx, '查找明星失败', err)
            logUtil.logInfo('查找明星失败' + err);
        }
    }
    static async everyMonth() {
        try {
            let pagination = _.cloneDeep(tool.pageModel)
            pagination.pagination = false
            let rows = await model.findByCondition({}, pagination)
            _.forEach(rows, async (row, i) => {
                row.hitFans.map((item) => {
                    item.hitNumb = 0
                })
                await row.save()
            })
            logUtil.logInfo('月统计成功');
        } catch (err) {
            logUtil.logInfo('月统计出错' + err);
        }
    }
    /**
     * 通过ID返回用户对象
     * @param {*} ctx 
     */
    static async find(ctx) {
        try {
            let [startTime, args] = [new Date().getTime(), ctx.request.body]
            let [condition, pagination] = [args.condition || {}, args.pagination || {}]
            // 转化模糊搜索
            tool.toRegExp(condition, 'name')
            // 设置默认参数
            pagination = _.assign(_.cloneDeep(tool.pageModel), pagination)
            let rows = await model.findByCondition(condition, pagination)
            let count = await model.countByCondition(condition)
            let result = {
                page: parseInt(pagination.page),
                total: _.ceil(count / pagination.rows),
                records: count,
                ms: new Date() - startTime,
                rows: rows,
            }
            tool.resSuccess(ctx, result)
        } catch (err) {
            tool.resError(ctx, '查找明星失败', err)
            logUtil.logInfo('查找明星失败' + err);
        }
    }
    /**
     * 新增app类型信息接口
     * @param {*} ctx 
     */
    static async insert(ctx) {
        try {
            let args = ctx.request.body;
            // 判断name重复
            let one = await model.findOne({
                name: args.name
            })
            if (one) throw '这个明星团已存在'
            // 保存
            var result = await new model(args).save();
            tool.resSuccess(ctx, '新增成功');
        } catch (err) {
            tool.resError(ctx, '新增明星失败', err)
            logUtil.logInfo('新增明星失败' + err)
        }
    }
    /**
     * 删除app类型信息接口
     * @param {*} ctx 
     */
    static async delete(ctx) {
        try {
            let args = ctx.request.body;
            let one = await model.findById(args._id);
            if (one) one.remove()
            else throw '未找到对应记录'
            tool.resSuccess(ctx, `成功删除`);
        } catch (err) {
            tool.resError(ctx, '删除失败', err)
            logUtil.logInfo('删除错误：' + err)
        }
    }
    /**
     * 修改app类型信息接口
     * @param {*} ctx 
     */
    static async update(ctx) {
        try {
            let args = ctx.request.body;
            let condition = _.pick(args, ['_id'])
            let option = _.omit(args, ['_id'])
            var result = await model.update(condition, option);
            if (result.ok == 0) {
                tool.resError(ctx, '修改出错', '未找到对应记录');
            } else {
                tool.resSuccess(ctx, `修改成功`);
            }
        } catch (err) {
            tool.resError(ctx, '修改出错', err);
            logUtil.logInfo('修改出错' + err);
        }
    }
    // 对外API
    /**
     * 模糊查询明星团
     * @param {*} ctx
     * @param {string} ctx.name
     */
    static async findStarByName(ctx) {
        try {
            let args = ctx.request.body;
            if (!args.name) throw '查询名称不得为空'
            let nameReg = new RegExp(args.name, 'i')
            let pagination = {
                pagination: false
            }
            pagination = _.assign(_.cloneDeep(tool.pageModel), pagination)
            var result = await model.findByCondition({
                name: nameReg
            }, pagination, {
                'name': 1,
                'type': 1,
                'userimg': 1,
                'poster': 1,
                'sort': 1,
                'hot': 1
            });
            let date = moment().format('YYYYMM').toString()
            let list = _.clone(result)
            list.map(item => {
                item.hot = item.hot[date] || 0
                return item
            })
            tool.resSuccess(ctx, list);
        } catch (err) {
            tool.resError(ctx, '获取失败', err);
            logUtil.logInfo('获取失败' + err);
        }
    }
    /**
     * 热门排名接口
     * @param {*} ctx 
     * @param {string} ctx.date 例如:201807 ,默认本月
     */
    static async findStarSort(ctx) {
        try {
            let startTime = new Date().getTime();
            let args = ctx.request.body;
            let condition = args.condition || {}
            let pagination = args.pagination || {}
            args.date = args.date || new Date()
            let week = args.week
            let date = moment(args.date).format('YYYYMM').toString()
            pagination = _.assign(_.cloneDeep(tool.pageModel), pagination)
            if (week === 0) {
                pagination.sortname = `weekHot`
            } else if (week === -1) {
                pagination.sortname = `lastWeekHot`
            } else {
                pagination.sortname = `hot.${date}`
            }
            let result = await model.findByCondition(condition, pagination, {
                'name': 1,
                'type': 1,
                'sort': 1,
                'userimg': 1,
                'poster': 1,
                'hot': 1,
                'weekHot': 1,
                'lastWeekHot': 1
            });
            let list = _.clone(result)
            if (week === 0) {
                list.map(item => {
                    item.hot = item.weekHot
                    return item
                })
            } else if (week === -1) {
                list.map(item => {
                    item.hot = item.lastWeekHot
                    return item
                })
            } else {
                list.map(item => {
                    item.hot = item.hot[date] || 0
                    return item
                })
            }
            let count = await model.countByCondition(condition)
            let res = {
                page: parseInt(pagination.page),
                total: _.ceil(count / pagination.rows),
                records: count,
                ms: new Date() - startTime,
                rows: list,
            }
            tool.resSuccess(ctx, res);
        } catch (err) {
            tool.resError(ctx, '获取失败', err);
            logUtil.logInfo('获取失败' + err);
        }
    }
    /**
     * 发表弹幕
     * @param {*} ctx 
     * @param {*} ctx.starid 明星团id 
     * @param {*} ctx.openid 发表人id
     * @param {*} ctx.value 弹幕内容
     */
    static async sendWord(ctx) {
        try {
            let args = ctx.request.body;
            tool.isHas(args, ['starid', 'openid', 'value'])
            let wxuser = await wxUser.findOne({
                openid: args.openid
            })
            if (!wxuser) throw '该用户不存在'
            let star = await model.findById(args.starid);
            if (!star) throw '该明星团不存在'
            if (star.words.length > 200) throw '弹幕上限200条'
            star.words.push({
                openid: wxuser.openid,
                name: wxuser.nickName,
                avatarUrl: wxuser.avatarUrl,
                value: args.value
            })
            star.save()
            tool.resSuccess(ctx, '发送成功');
        } catch (err) {
            tool.resError(ctx, '发送失败', err);
            logUtil.logInfo('发送失败' + err);
        }
    }
    /**
     * 获取明星团热度
     * @param {*} ctx 
     * @param {string} ctx.id 用户团id
     */
    static async findHotById(ctx) {
        try {
            let args = ctx.request.body;
            let result = await methodFindHot(args.id, args)
            tool.resSuccess(ctx, result);
        } catch (err) {
            tool.resError(ctx, '获取失败', err);
            logUtil.logInfo('获取失败' + err);
        }
    }

        /**
     * 获取明星团热度
     * @param {*} ctx 
     * @param {Array} ctx.id 用户团id集合
     */
    static async findHotByIds(ctx) {
        try {
            let args = ctx.request.body;
            if(!args.ids || args.ids.constructor !== Array) throw '参数错误'
            let result = await methodFindHots(args.ids, args)
            tool.resSuccess(ctx, result);
        } catch (err) {
            tool.resError(ctx, '获取失败', err);
            logUtil.logInfo('获取失败' + err);
        }
    }

    /**
     * 快报接口
     * @param {*} ctx 
     */
    static async findKuaibao(ctx) {
        try {
            var result = await model.find({}, {
                _id: 1,
                name: 1,
                userimg: 1
            }).sort({
                sort: -1,
                'meta.updateAt': -1
            }).limit(10)
            tool.resSuccess(ctx, result);
        } catch (err) {
            tool.resError(ctx, '获取失败', err);
            logUtil.logInfo('获取失败' + err);
        }
    }
    /**
     * 明星团详情接口
     * @param {*} ctx 
     * @param {string} ctx.id 明星团id
     * @param {number} ctx.n 粉丝排行长度 默认：5
     */
    static async findStarById(ctx) {
        try {
            let args = ctx.request.body;
            if (!args.id) throw '缺少id参数'
            var result = await model.findById(args.id)
            if (!result) throw '未找到对应记录'
            tool.resSuccess(ctx, result);
        } catch (err) {
            tool.resError(ctx, '获取失败', err);
            logUtil.logInfo('获取失败' + err);
        }
    }
    /**
     * 粉丝打榜
     * @param {*} ctx 
     * @param {*} ctx.openid 微信用户id
     * @param {*} ctx.starid 明星团id
     * @param {*} ctx.count 积分数
     */
    static async hitFans(ctx) {
        try {
            let args = ctx.request.body
            tool.isHas(args, ['starid', 'openid', 'count'])
            let {
                openid,
                starid,
                count
            } = args
            count = parseInt(count)
            /* 判断是否 存在 两个对象 */
            let wxuser = await wxUser.findOne({
                openid: openid
            })
            if (!wxuser) throw '打榜失败，用户不存在'

            let star = await model.findById(starid)
            if (!star) throw '打榜失败，明星团不存在'
            rStarAndWxUser.dabang({
                openid,
                starid,
                nickName: wxuser.nickName,
                avatarUrl: wxuser.avatarUrl,
                count
            })
            // 热度增加
            let date = moment().format('YYYYMM').toString()
            let obj = _.clone(star.hot)
            if (_.has(obj, date)) {
                obj[date] = parseInt(obj[date]) + count
            } else {
                obj[date] = count
            }
            star.weekHot += count
            star.hot = obj
            star.save()
            /******/
            /* 记录打榜记录 */
            let rechargeOne = {
                openid,
                starid,
                count
            }
            if(count!=1) await new recharge(rechargeOne).save()
            /******/
            /* 用户打赏更新 */
            let findResult = _.find(wxuser.stars, {
                name: star.name
            })
            if (findResult) {
                findResult.hitTime = new Date()
            } else {
                wxuser.stars.splice(0, 0, {
                    id: starid,
                    name: star.name,
                    type: star.type,
                    sort: star.sort,
                    userimg: star.userimg,
                    hitTime: new Date()
                })
                if (wxuser.stars.length > 20) wxuser.stars.length = 20
            }
            wxuser.stars = _.orderBy(wxuser.stars, ['hitTime'], ['desc'])
            try{
                //console.log("wxuserSave",starid)
                await wxuser.save()
            }
            catch(e){
                throw e
            }
            /******/
            tool.resSuccess(ctx, {
                data: '打榜成功',
                count
            });
        } catch (err) {
            tool.resError(ctx, '打榜失败', err);
            logUtil.logInfo('打榜失败' + err);
        }
    }

    /**
     * 粉丝应援
     * @param {*} ctx 
     * @param {*} ctx.type 应援类型
     * @param {*} ctx.field 应援字段
     * @param {*} ctx.openid 微信用户id
     * @param {*} ctx.starid 明星团id
     * @param {*} ctx.count 积分数
     * @param {*} ctx.rep 反馈内容
     */
    static async hitActivity(ctx){
        try {
            let args = ctx.request.body
            tool.isHas(args, ['starid', 'openid', 'count','field','rep'])
            let {
                openid,
                starid,
                count,
                field,
                rep
            } = args
            count = parseInt(count)
            /* 判断是否 存在 两个对象 */
            let wxuser = await wxUser.findOne({
                openid: openid
            })
            if (!wxuser) throw '应援失败，用户不存在'

            let star = await model.findById(starid)
            if (!star) throw '应援失败，明星团不存在'
            // 热度增加
            var curCount = _.clone(star[field])
            curCount += count 
            star[field] = curCount
            star.save()
            /******/
            // console.log('成功',rep)
            tool.resSuccess(ctx, {
                data: rep,
                count
            });
        } catch (err) {
            tool.resError(ctx, '应援失败', err);
            logUtil.logInfo('应援失败' + err);
        }
    }
}

const relate = require('../db/models/rStarAndWxUser')
async function change() {
    let all = await model.find({});
    for (var one of all) {
        let starid = one._id
        for (fans of one.hitFans) {
            let openid = fans.openid
            let hitNumb = fans.hitNumb
            let allHitNumb = fans.allHitNumb
            let nickName = fans.nickName
            let avatarUrl = fans.avatarUrl
            await relate({
                openid,
                starid,
                nickName,
                avatarUrl,
                hitNumb,
                allHitNumb
            }).save();
        }
    }
}

Controller.methodFindHot = methodFindHot
Controller.methodFindHots = methodFindHots
module.exports = Controller;

// change();