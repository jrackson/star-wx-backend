const model = require('../db/models/wxUser')
const starModel = require('./StarController')
const tool = require('../utils/tool')
const logUtil = require('../utils/log_util')
const _ = require('lodash')
class Controller {
    /**
     * 通过ID返回用户对象
     * @param {*} ctx 
     */
    static async find(ctx) {
        try {
            let [startTime, args] = [new Date().getTime(), ctx.request.body]
            let [condition, pagination] = [args.condition || {}, args.pagination || {}]
            // 转化模糊搜索
            tool.toRegExp(condition, 'nickName')
            // 设置默认参数
            pagination = _.assign(_.cloneDeep(tool.pageModel), pagination)
            let rows = await model.findByCondition(condition, pagination)
            let count = await model.countByCondition(condition)
            let result = {
                page: parseInt(pagination.page),
                total: _.ceil(count / pagination.rows),
                records: count,
                ms: new Date() - startTime,
                rows: rows,
            }
            tool.resSuccess(ctx, result)
        } catch (err) {
            tool.resError(ctx, '查找失败', err)
            logUtil.logInfo('查找失败' + err);
        }
    }
    /**
     * 新增app类型信息接口
     * @param {*} ctx 
     */
    static async insert(ctx) {
        try {
            let args = ctx.request.body;
            // 判断name重复
            let one = await model.findOne({
                openid: args.openid
            })
            if (one) throw '这个记录已存在'
            // 保存
            var result = await new model(args).save();
            tool.resSuccess(ctx, result);
        } catch (err) {
            tool.resError(ctx, '新增失败', err)
            logUtil.logInfo('新增失败' + err)
        }
    }
    /**
     * 删除app类型信息接口
     * @param {*} ctx 
     */
    static async delete(ctx) {
        try {
            let args = ctx.request.body;
            let one = await model.findOne(args);
            if (one) one.remove()
            else throw '未找到对应记录'
            tool.resSuccess(ctx, `成功删除`);
        } catch (err) {
            tool.resError(ctx, '删除失败', err)
            logUtil.logInfo('删除错误：' + err)
        }
    }
    /**
     * 修改app类型信息接口
     * @param {*} ctx 
     */
    static async update(ctx) {
        try {
            let args = ctx.request.body;
            let condition = _.pick(args, ['_id'])
            let option = _.omit(args, ['_id'])
            var result = await model.update(condition, option);
            if (result.ok == 0) {
                tool.resError(ctx, '修改出错', '未找到对应记录');
            } else {
                tool.resSuccess(ctx, `修改成功`);
            }
        } catch (err) {
            tool.resError(ctx, '修改出错', err);
            logUtil.logInfo('修改出错' + err);
        }
    }
    // 供API使用
    /**
     * 获取用户详情
     * @param {*} ctx 
     * @param {string} ctx.openid 微信用户id
     * @param {number} ctx.n 显示打榜明星数量 （默认：5）
     */
    static async findUserById(ctx) {
        try {
            let args = ctx.request.body;
            tool.isHas(args, ['openid'])
            let result = await model.findOne({
                openid: args.openid
            })
            if (!result) throw '未找到对应记录'
            let n = args.n || 5
            result.stars = _.take(result.stars, n)
            if(result.stars.length>0){
                let ids = result.stars.map(v=>{
                    return {'_id':v.id}
                })
                let hotList = await starModel.methodFindHots(ids)
                result.stars.forEach(v=>{
                    let item = hotList.find(p=>p._id.toString()== v.id)
                    if(item){
                        v.hot = item.hot,
                        v.sort = item.sort
                    } else {
                        console.log('ObjectId',v.id,hotList)
                    }
                })
                // for (let item of result.stars) {
                //     let starid = item.id
                //     let hotObj = await starModel.methodFindHot(starid)
                //     item.hot = hotObj.hot
                //     item.sort = hotObj.sort
                // }
            }
            tool.resSuccess(ctx, result);
        } catch (err) {
            tool.resError(ctx, '获取出错', err);
            logUtil.logInfo('获取出错' + err);
        }
    }
    static async findUidByOpenid(ctx) {
        try {
            let args = ctx.request.body;
            tool.isHas(args, ['unionId'])
            let result = await model.findOne({
                unionId: args.unionId
            })
            if (!result) throw '未找到对应记录'
            tool.resSuccess(ctx, {
                openid: result.openid
            });
        } catch (err) {
            tool.resError(ctx, '获取出错', err);
            logUtil.logInfo('获取出错' + err);
        }
    }
}

module.exports = Controller;