const LogResponse = require('../db/models/logResponse')
const logUtil = require('../utils/log_util')
class LogDB {
    static async insert(ctx, ms) {
        var req = ctx.request;
        var model = {
            reqOriginalUrl: req.originalUrl,
            reqMethod: req.method,
            reqClientIP: req.clientIP,
            reqQuery: req.query,
            reqBody: req.body,
            resbody: ctx.body,
            resTime: ms,
            resStatus: ctx.status,
        }
        var logResponse = new LogResponse(model)
        try {
            await logResponse.save();
        } catch (err) {
            logUtil.logInfo('错误：' + err)
        }
    }
    static async do() {
        /* 更新返回对象 */
        // LogResponse.findById('5b18fe56f23255db772531cb', function (err, logResponse) {
        //     if (err) return handleError(err);
        //     logResponse.set({
        //         reqClientIP: '127.0.0.1'
        //     });
        //     logResponse.save(function (err, updatedLogResponse) {
        //         if (err) return handleError(err);
        //         console.log(updatedLogResponse);
        //     });
        // });

        /* 更新返回{n:1,nModified:1,ok:1},如果添加模型中未定义的字段 */
        // LogResponse.schema.add({
        //     sss: 'string'
        // });
        // LogResponse.update({
        //     _id: '5b18fe56f23255db772531cb'
        // }, {
        //     $set: {
        //         reqClientIP: '127.0.0.2',
        //         sss: '11'
        //     }
        // }, function (err, updatedLog) {
        //     console.log(updatedLog)
        //     LogResponse.schema.remove('sss')
        // });

        /* 删除一条记录 {n:1,ok:1} 不触发pre('remove') */
        // LogResponse.deleteOne({
        //     _id: '5b18feae2bd4cedb8dbdefc5'
        // }, function (err, result) {
        //     console.log(result)
        // });

        /* 根据model删除记录 */
        // LogResponse.findById('5b1a22442b4a82e0e6baa766', function (err, logResponse) {
        //     if (err) return handleError(err);
        //     if (logResponse)
        //         logResponse.remove(function (err, logResponse) {
        //             if (err) return handleError(err);
        //             LogResponse.findById(logResponse._id, function (err, product) {
        //                 console.log(product) // null
        //             })
        //         });
        // });
    }
}

module.exports = LogDB;

// LogDB.do()