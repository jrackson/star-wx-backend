const model = require('../db/models/rStarAndWxUser')
const tool = require('../utils/tool')
const logUtil = require('../utils/log_util')
const _ = require('lodash')
class Controller {
    static async everyMonth() {
        try {
            let pagination = _.cloneDeep(tool.pageModel)
            pagination.pagination = false
            let rows = await model.findByCondition({
                weekNumb: {
                    $exists: true
                }
            }, pagination, {
                weekNumb: 1
            })
            _.forEach(rows, async (row, i) => {
                row.hitNumb = 0
                await row.save()
            })
            logUtil.logInfo('月统计成功');
        } catch (err) {
            logUtil.logInfo('月统计出错' + err);
        }
    }
    static async everyWeek() {
        try {
            let pagination = _.cloneDeep(tool.pageModel)
            pagination.pagination = false
            let rows = await model.findByCondition({
                weekNumb: {
                    $exists: true
                }
            }, pagination, {
                weekNumb: 1
            })
            _.forEach(rows, async (row, i) => {
                row.weekNumb = 0
                await row.save()
            })
            console.log('周统计成功');
        } catch (err) {
            logUtil.logInfo('周统计出错' + err);
        }
    }
    /**
     * 通过ID返回用户对象
     * @param {*} ctx 
     */
    static async find(ctx) {
        try {
            let [startTime, args] = [new Date().getTime(), ctx.request.body]
            let [condition, pagination] = [args.condition || {}, args.pagination || {}]
            // 转化模糊搜索
            // tool.toRegExp(condition, 'title')
            // 设置默认参数
            pagination = _.assign(_.cloneDeep(tool.pageModel), pagination)
            // console.log(pagination)
            let rows = await model.findByCondition(condition, pagination)
            let count = await model.countByCondition(condition)
            let result = {
                page: parseInt(pagination.page),
                total: _.ceil(count / pagination.rows),
                records: count,
                ms: new Date() - startTime,
                rows: rows,
            }
            tool.resSuccess(ctx, result)
        } catch (err) {
            tool.resError(ctx, '查找失败', err)
            logUtil.logInfo('查找失败' + err);
        }
    }
    /**
     * 新增app类型信息接口
     * @param {*} ctx 
     */
    static async insert(ctx) {
        try {
            let args = ctx.request.body;
            // tool.isHas(args, ['starid', 'openid', 'content'])
            // 判断name重复
            let one = await model.findOne({
                title: args.title
            })
            if (one) throw '这个记录已存在'
            // 保存
            let star = await starModel.findById(args.starid);
            if (!star) throw '未找到明星团'
            let news = await new model(args).save();
            star.news.splice(0, 0, news._id)
            await star.save()
            tool.resSuccess(ctx, '添加成功');
        } catch (err) {
            tool.resError(ctx, '新增失败', err)
            logUtil.logInfo('新增失败' + err)
        }
    }
    /**
     * 删除app类型信息接口
     * @param {*} ctx 
     */
    static async delete(ctx) {
        try {
            let args = ctx.request.body;
            let one = await model.findById(args._id);
            if (!one) throw '未找到对应记录one'
            await one.remove()
            tool.resSuccess(ctx, `成功删除`);
        } catch (err) {
            tool.resError(ctx, '删除失败', err)
            logUtil.logInfo('删除错误：' + err)
        }
    }
    /**
     * 修改app类型信息接口
     * @param {*} ctx 
     */
    static async update(ctx) {
        try {
            let args = ctx.request.body;
            let condition = _.pick(args, ['_id'])
            let option = _.omit(args, ['_id'])
            var result = await model.update(condition, option);
            if (result.ok == 0) {
                tool.resError(ctx, '修改出错', '未找到对应记录');
            } else {
                tool.resSuccess(ctx, `修改成功`);
            }
        } catch (err) {
            tool.resError(ctx, '修改出错', err);
            logUtil.logInfo('修改出错' + err);
        }
    }
    // 打榜
    static async dabang(args) {
        try {
            let {
                openid,
                starid,
                count,
                nickName,
                avatarUrl
            } = args
            count = ~~count
            let one = await model.findOne({
                openid,
                starid
            })
            // console.log(one)
            if (one) {
                one.hitNumb += count
                one.allHitNumb += count
                one.weekNumb += count
            } else {
                one = new model({
                    openid,
                    starid,
                    nickName,
                    avatarUrl,
                    hitNumb: count,
                    weekNumb: count,
                    allHitNumb: count
                })
            }
            await one.save();
        } catch (err) {
            logUtil.logInfo('查找失败' + err);
        }
    }
}


// Controller.everyWeek()
module.exports = Controller;