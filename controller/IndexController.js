const tool = require('../utils/tool')
const config = require('../config/web_config')
const moment = require('moment')
class IndexController {
    /**
     * 首页
     * @param {*} ctx 
     */
    static async api(ctx) {
        await ctx.render('index', {
            title: 'API详情',
            routerInfo: require('../cache/routerInfoCache').routerInfo
        })
    }
    /**
     * 返回连接状态
     * @param {*} ctx 
     */
    static async conn(ctx) {
        tool.resSuccess(ctx, require('../cache/conStateCache').connectionState)
    }
    /**
     * 返回当前服务器时间
     * @param {*} ctx 
     */
    static async time(ctx) {
        tool.resSuccess(ctx, new Date())
    }
    static async downloadUrl(ctx) {
        tool.resSuccess(ctx, config.downloadUrl)
    }
    static async endWeek(ctx) {
        tool.resSuccess(ctx, moment().endOf('week').valueOf())
    }
}

module.exports = IndexController;