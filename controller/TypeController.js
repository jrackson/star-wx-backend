const model = require('../db/models/type')
const tool = require('../utils/tool')
const logUtil = require('../utils/log_util')
const _ = require('lodash')
class Controller {
    /**
     * 通过ID返回用户对象
     * @param {*} ctx 
     */
    static async find(ctx) {
        try {
            let [startTime, args] = [new Date().getTime(), ctx.request.body]
            let [condition, pagination] = [args.condition || {}, args.pagination || {}]
            // 转化模糊搜索
            tool.toRegExp(condition, 'name')
            // 设置默认参数
            pagination = _.assign(_.cloneDeep(tool.pageModel), pagination)
            let rows = await model.findByCondition(condition, pagination)
            let count = await model.countByCondition(condition)
            let result = {
                page: parseInt(pagination.page),
                total: _.ceil(count / pagination.rows),
                records: count,
                ms: new Date() - startTime,
                rows: rows,
            }
            tool.resSuccess(ctx, result)
        } catch (err) {
            tool.resError(ctx, '查找失败', err)
            logUtil.logInfo('查找失败' + err);
        }
    }
    /**
     * 新增app类型信息接口
     * @param {*} ctx 
     */
    static async insert(ctx) {
        try {
            let args = ctx.request.body;
            // 判断name重复
            let one = await model.findOne({
                name: args.name
            })
            if (one) throw '这个记录已存在'
            // 保存
            var result = await new model(args).save();
            tool.resSuccess(ctx, result);
        } catch (err) {
            tool.resError(ctx, '新增失败', err)
            logUtil.logInfo('新增失败' + err)
        }
    }
    /**
     * 删除app类型信息接口
     * @param {*} ctx 
     */
    static async delete(ctx) {
        try {
            let args = ctx.request.body;
            let one = await model.findById(args._id);
            if (one) one.remove()
            else throw '未找到对应记录'
            tool.resSuccess(ctx, `成功删除`);
        } catch (err) {
            tool.resError(ctx, '删除失败', err)
            logUtil.logInfo('删除错误：' + err)
        }
    }
    /**
     * 修改app类型信息接口
     * @param {*} ctx 
     */
    static async update(ctx) {
        try {
            let args = ctx.request.body;
            let condition = _.pick(args, ['_id'])
            let option = _.omit(args, ['_id'])
            var result = await model.update(condition, option);
            if (result.ok == 0) {
                tool.resError(ctx, '修改出错', '未找到对应记录');
            } else {
                tool.resSuccess(ctx, `修改成功`);
            }
        } catch (err) {
            tool.resError(ctx, '修改出错', err);
            logUtil.logInfo('修改出错' + err);
        }
    }
    static async findTypes(ctx) {
        try {
            let result = await model.find({}, {
                name: 1
            })
            tool.resSuccess(ctx, result);
        } catch (err) {
            tool.resError(ctx, '获取失败', err)
            logUtil.logInfo('获取失败' + err)
        }
    }
}

module.exports = Controller;