const crypto = require('crypto')
const tool = require('../utils/tool')
let sha1 = message => {
    return crypto.createHash('sha1').update(message, 'utf8').digest('hex')
}

function checkSignature(signature, timestamp, nonce) {
    const tmpStr = ['xiaokefu', timestamp, nonce].sort().join('')
    const sign = sha1(tmpStr)
    return sign === signature
}

async function post(ctx, next) {
    // 检查签名，确认是微信发出的请求
    const {
        signature,
        timestamp,
        nonce
    } = ctx.query
    if (!checkSignature(signature, timestamp, nonce))
        ctx.body = 'ERR_WHEN_CHECK_SIGNATURE'
    else {
        /**
         * 解析微信发送过来的请求体
         * 可查看微信文档：https://mp.weixin.qq.com/debug/wxadoc/dev/api/custommsg/receive.html#接收消息和事件
         */
        const body = ctx.request.body
        let url = 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=xiaokefu'
        let result = await tool.httpPost(url, {
            "touser": body.FromUserName,
            "msgtype": "text",
            "text": {
                "content": '<a href="https://mp.weixin.qq.com/s/-pXzb9uc9GecP78fA59uGw"></a>'
            }
        })
        ctx.body = 'success'
    }
}

module.exports = {
    post
}