const tool = require('../utils/tool')
const user = require('../db/models/user')
const uuidv1 = require('uuid/v1')
const moment = require('moment')
class UserController {
    /**
     * 验证用户名密码 并更新token
     * @param {*} ctx 
     */
    static async validate(ctx) {
        let param = ctx.request.body
        let username = param.username
        let secret = param.secret
        try {
            let one = await user.findByName(username)
            if (one) {
                // 验证密码
                if (one.secret === secret) {
                    let token = uuidv1()
                    let expire = moment().add('d', 1)
                    one.token = {
                        value: tool.md5(token),
                        expire: expire
                    }
                    // 更新token
                    await one.save()
                    // 更新cookie中token
                    ctx.cookies.set('token', JSON.stringify({
                        username: username,
                        value: token
                    }), {
                        expires: expire.toDate(),
                        httpOnly: false
                    })
                    tool.resSuccess(ctx, '登陆成功')
                } else
                    tool.resError(ctx, '用户名或密码错误')
            } else {
                tool.resError(ctx, '该用户不存在')
            }
        } catch (err) {
            tool.resError(ctx, '请求错误', err)
        }
    }
    /**
     * 获取所有用户对象
     * @param {*} ctx 
     */
    static async token(ctx) {
        let param = ctx.request.body
        let username = param.username
        let token = param.token
        let one = await user.findByNameAndToken(username, token)
        if (one) {
            if (one.token.expire.getTime() > new Date().getTime())
                tool.resSuccess(ctx, '授权成功')
            else
                tool.resError(ctx, '授权过期')
        } else {
            tool.resError(ctx, '校验失败')
        }
    }

    static async newUser(ctx) {
        let param = ctx.request.body
        let username = param.username
        let secret = param.secret
        let newOne = new user({
            name: username,
            secret: secret,
        })
        let result = await newOne.save()
        tool.resSuccess(ctx, result)
    }
    /**
     * 修改密码
     * @param {*} ctx 
     */
    static async changePwd(ctx) {
        let param = ctx.request.body
        let secret = param.secret
        await user.findOneAndUpdate({
            name: ctx.state.username
        }, {
            secret: secret
        })
        tool.resSuccess(ctx, '修改密码成功')
    }
}

module.exports = UserController;
