const model = require('../db/models/activity')
const starModel = require('../db/models/star')
const wxUser = require('../db/models/wxUser')
const starController = require('./StarController')
const tool = require('../utils/tool')
const logUtil = require('../utils/log_util')
const moment = require('moment')
const _ = require('lodash')

/**
 * 应援反馈模板
 */
const template = {
    touping:{
        name:'投屏',
        require:10000,
        award:'我们将安排偶像的视频或者海报投放在南京新街口中央商场LED大屏幕上',
        field:'touping'
    },
    shengri:{
        name:'生日',
        award:'祝福我们的爱豆永远有我们守护,幸福美满',
        field:'shengri'
    },
    biaoqing:{
        name:'表情',
        require:10000,
        award:'我们将安排专业的微信表情绘制专家为爱豆定制一套微信表情,永久存储在微信表情库里',
        field:'biaoqing'
    },
}

/**
 * 应援反馈生成
 * @param {*} result 参与历史反馈
 * @param {*} result.join 是否参加过该类型的应援
 * @param {*} result.name 历史参加的应援明星姓名
 * @param {*} result.sj 历史参加的应援时间
 * @param {*} name 应援明星姓名
 * @param {*} type 应援类型
 * @param {*} count 明星应援次数
 */
async function repMsg (result,name,type,count){
    var rep = (!result.join)?`恭喜,成功为爱豆${name}参与${template[type].name}应援活动!`:`抱歉,你已于${result.sj}参与过爱豆${result.name}的${template[type].name}应援活动！`
    rep += '\n'
    rep += `目前${name}已获得有效应援${count}次` 
    rep +=  template[type].require ? (template[type].require - count > 0 ?`,还差${template[type].require - count}次`:',已完成目标'):''
    rep += template[type].award ? `${template[type].require ?',达成目标后':''},${template[type].award},请召集更多的粉丝前来应援！` :''
    return rep
}

/**
 * 判断是否已经参与
 * @param {*} args 
 * @param {*} args.openid 用户id
 * @param {*} args.type 应援类型
 * @param {*} args.range 范围
 */
async function isJoin(args) {
    //console.log('isJoinEnter')
    let {
        openid,
        type,
        range
    } = args;
    let activityOne = await model.findOne({
        openid,
        type,
        'meta.updateAt': {
            $gt: range[0],
            $lt: range[1]
        }
    })
    if(activityOne){
        let starOne = await starModel.findById({
            _id:activityOne.starid
        })
        let name = starOne? starOne.name:'未知明星'
        return  {
            join:true,
            sj:moment(activityOne.meta.createAt).format('YYYY年MM月DD日 hh时mm分ss秒'),
            name
        }
    }
    else{
        return{
            join:false
        }
    }
}

class Controller{
    /**
     * 参与
     * @param {*} ctx 
     * @param {*} ctx.openid 用户id
     * @param {*} ctx.type 应援类型
     * @param {*} ctx.star 应援明星对象
     */
    static async join(ctx){
        try {          
            let args = ctx.request.body
            tool.isHas(args, ['openid', 'star','type'])
            let star = args.star
            args = _.pick(args, ['openid','type'])
            let date = moment().format('YYYY-MM-DD')
            let result = await isJoin({
                ...args,
                range: [`2017-12-12 00:00:00`, `${date} 23:59:59`]
            })
            var count = star[template[args.type].field] ? star[template[args.type].field] :0
            if (!result.join) {
                args = {
                    ...args,
                    starid:star._id
                }
                await new model(args).save();
                // 应援+1
                count += 1
            }
            var rep = await repMsg(result,star.name,args.type,count)
            if(!result.join){
                // 'starid', 'openid', 'count','field','rep']
                ctx.request.body.count = 1
                ctx.request.body.rep = rep
                ctx.request.body.starid = star._id
                ctx.request.body.field = template[args.type].field
                
                await starController.hitActivity(ctx)
            }else{
                // console.log("成功",rep)
                tool.resSuccess(ctx, {
                    data: rep,
                    count
                });
            }
        } catch (err) {
            tool.resError(ctx, '获取失败', err)
            logUtil.logInfo('获取失败' + err)
        }
    }

    /**
     * 应援接口
     * @param {*} ctx
     * @param {*} ctx.name 应援明星姓名
     * @param {*} ctx.unionId 用户唯一标识
     * @param {*} ctx.type 应援类型 
     */
    static async activity(ctx) {
        try {
            
            let args = ctx.request.body
            tool.isHas(args, ['name', 'unionId','type'])
            let {
                name,
                unionId,
                type
            } = args
            //console.log('activityEnter',name)
            if(!template[type]) throw '错误的应援方式'
            let starOne = await starModel.findOne({
                name
            })
            if (!starOne) throw '没有该明星团'
            let userOne = await wxUser.findOne({
                unionId
            })
            if (!userOne) throw '没有该角色'

            let star = _.cloneDeep(starOne)
            let openid = userOne.openid

            ctx.request.body = {
                star,
                openid,
                type
            }
            await Controller.join(ctx)
        } catch (err) {
            tool.resError(ctx, '出错', err);
            logUtil.logInfo('出错' + err);
        }
    }
}

module.exports = Controller