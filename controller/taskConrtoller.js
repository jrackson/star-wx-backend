const tool = require('../utils/tool')
const logUtil = require('../utils/log_util')
const moment = require('moment')
const model = require('../db/models/sign')

async function isSign2(args) {
    let {
        openid,
        type,
        range
    } = args;
    let count = await model.countDocuments({
        openid,
        type,
        'meta.updateAt': {
            $gt: range[0],
            $lt: range[1]
        }
    })
    return !!count
}
async function isSign(args) {
    let {
        openid,
        starid,
        type,
        range
    } = args;
    let count = await model.countDocuments({
        openid,
        starid,
        type,
        'meta.updateAt': {
            $gt: range[0],
            $lt: range[1]
        }
    })
    return !!count
}
async function isSignBack(args) {
    let {
        openid,
        starid,
        type,
        range
    } = args;
    let rows = await model.find({
        openid,
        starid,
        type,
        'meta.updateAt': {
            $gt: range[0],
            $lt: range[1]
        }
    })
    return rows
}
class Controller {
    /**
     * 通过OpenId,StarId返回任务对象
     * @param {*} ctx 
     */
    static async getTask(ctx) {
        try {
            let date = moment().format('YYYY-MM-DD')
            let args = ctx.request.body;
            //console.log(args)
            if (!args.openid) throw '用户id不能为空'
            if (!args.starid) throw '明星id不能为空'
            var shareResult = await isSignBack({
                openid: args.openid,
                starid: args.starid,
                type: "分享",
                range: [`${date} 00:00:00`, `${date} 23:59:59`]
            })
            var platfromResult = await isSign2({
                openid: args.openid,
                type: "守护",
                range: [`2017-12-12 00:00:00`, `${date} 23:59:59`]
            })
            var data = {
                share: shareResult.length,
                platform: platfromResult
            }
            tool.resSuccess(ctx, data);

        } catch (err) {
            tool.resError(ctx, '查找失败', err)
            logUtil.logInfo('查找失败' + err)
        }
    }
}

module.exports = Controller;