const model = require('../db/models/sign')
const star = require('../db/models/star')
const wxUser = require('../db/models/wxUser')
const starController = require('./StarController')
const tool = require('../utils/tool')
const logUtil = require('../utils/log_util')
const moment = require('moment')
const reportList = require('../cache/reportListCache')
const _ = require('lodash')

const [_DABANG, _QIANDAO] = [1, 10]
async function isSign2(args) {
    let {
        openid,
        type,
        range
    } = args;
    let count = await model.countDocuments({
        openid,
        type,
        'meta.updateAt': {
            $gt: range[0],
            $lt: range[1]
        }
    })
    return !!count
}
async function isSign(args) {
    let {
        openid,
        starid,
        type,
        range
    } = args;
    let count = await model.countDocuments({
        openid,
        starid,
        type,
        'meta.updateAt': {
            $gt: range[0],
            $lt: range[1]
        }
    })
    return !!count
}
async function isSignBack(args) {
    let {
        openid,
        starid,
        type,
        range
    } = args;
    let rows = await model.find({
        openid,
        starid,
        type,
        'meta.updateAt': {
            $gt: range[0],
            $lt: range[1]
        }
    })
    return rows
}

function timeRange(time) {
    let date = moment().format('YYYY-MM-DD')
    let ranges = [
        [`${date} 00:00:00`, `${date} 05:59:59`],
        [`${date} 06:00:00`, `${date} 11:59:59`],
        [`${date} 12:00:00`, `${date} 17:59:59`],
        [`${date} 18:00:00`, `${date} 23:59:59`],
    ]
    let result = _.find(ranges, (range) => {
        return moment(time).isBetween(range[0], range[1])
    })
    return result
}


class Controller {
    /**
     * 通过ID返回用户对象
     * @param {*} ctx 
     */
    static async find(ctx) {
        try {
            let [startTime, args] = [new Date().getTime(), ctx.request.body]
            let [condition, pagination] = [args.condition || {}, args.pagination || {}]
            // 转化模糊搜索
            tool.toRegExp(condition, 'type')
            // 设置默认参数
            pagination = _.assign(_.cloneDeep(tool.pageModel), pagination)
            let rows = await model.findByCondition(condition, pagination)
            let count = await model.countByCondition(condition)
            let result = {
                page: parseInt(pagination.page),
                total: _.ceil(count / pagination.rows),
                records: count,
                ms: new Date() - startTime,
                rows: rows,
            }
            tool.resSuccess(ctx, result)
        } catch (err) {
            tool.resError(ctx, '查找失败', err)
            logUtil.logInfo('查找失败' + err);
        }
    }
    /**
     * 新增app类型信息接口
     * @param {*} ctx 
     */
    static async insert(ctx) {
        try {
            let args = ctx.request.body;
            // 保存
            var result = await new model(args).save();
            tool.resSuccess(ctx, result);
        } catch (err) {
            tool.resError(ctx, '新增失败', err)
            logUtil.logInfo('新增失败' + err)
        }
    }
    /**
     * 删除app类型信息接口
     * @param {*} ctx 
     */
    static async delete(ctx) {
        try {
            let args = ctx.request.body;
            let one = await model.findById(args._id);
            if (one) one.remove()
            else throw '未找到对应记录'
            tool.resSuccess(ctx, `成功删除`);
        } catch (err) {
            tool.resError(ctx, '删除失败', err)
            logUtil.logInfo('删除错误：' + err)
        }
    }
    /**
     * 修改app类型信息接口
     * @param {*} ctx 
     */
    static async update(ctx) {
        try {
            let args = ctx.request.body;
            let condition = _.pick(args, ['_id'])
            let option = _.omit(args, ['_id'])
            var result = await model.update(condition, option);
            if (result.ok == 0) {
                tool.resError(ctx, '修改出错', '未找到对应记录');
            } else {
                tool.resSuccess(ctx, `修改成功`);
            }
        } catch (err) {
            tool.resError(ctx, '修改出错', err);
            logUtil.logInfo('修改出错' + err);
        }
    }

    static async report(ctx) {
        try {
            let args = ctx.request.body;
            // let condition = _.pick(args, ['openid', 'formid'])
            let option = _.pick(args, ['openid', 'formid'])
            reportList.addReport(option)
            tool.resSuccess(ctx, `注册`);

        } catch (err) {
            tool.resError(ctx, '修改出错', err);
            logUtil.logInfo('修改出错' + err);
        }
    }

    static async shouhu(ctx) {
        try {
            let args = ctx.request.body
            tool.isHas(args, ['name', 'unionId'])
            let {
                name,
                unionId
            } = args
            let starOne = await star.findOne({
                name
            })
            if (!starOne) throw '没有该明星团'
            let userOne = await wxUser.findOne({
                unionId
            })
            if (!userOne) throw '没有该角色'
            let starid = starOne._id
            let openid = userOne.openid
            ctx.request.body = {
                starid,
                openid,
                type: '守护'
            }
            await Controller.sign(ctx)
        } catch (err) {
            tool.resError(ctx, '出错', err);
            logUtil.logInfo('出错' + err);
        }
    }

    static async sign(ctx) {
        try {
            let args = ctx.request.body
            tool.isHas(args, ['openid', 'starid', 'type'])
            args = _.pick(args, ['openid', 'starid', 'type'])
            let date = moment().format('YYYY-MM-DD')
            if (args.type == "打榜") {
                let range = timeRange(moment())
                let result = await isSign({ ...args,
                    range
                })
                if (result) throw '已经打过榜了'
                await new model(args).save();
                ctx.request.body.count = _DABANG
            } else if (args.type == "守护") {
                let result = await isSign2({
                    ...args,
                    range: [`2017-12-12 00:00:00`, `${date} 23:59:59`]
                })
                if (!result) {
                    await new model(args).save();
                    ctx.request.body.count = 100
                } else {
                    let result = await isSign2({
                        ...args,
                        range: [`${date} 00:00:00`, `${date} 23:59:59`]
                    })
                    if (result) throw '今日已经签到了'
                    await new model(args).save();
                    ctx.request.body.count = 1
                }
            } else if (args.type == "分享") {
                let openGId = ctx.request.body.openGId
                let result = await isSignBack({
                    ...args,
                    range: [`${date} 00:00:00`, `${date} 23:59:59`]
                })
                if (result.length > 4) throw '群分享上限5次'
                let as = _.find(result, (item) => {
                    return item.openGId == openGId
                })
                args.openGId = openGId
                if (as) throw '该群已经分享过了'
                await new model(args).save();
                ctx.request.body.count = _DABANG
            }
            await starController.hitFans(ctx)
        } catch (err) {
            tool.resError(ctx, '获取失败', err)
            logUtil.logInfo('获取失败' + err)
        }
    }
}

module.exports = Controller;