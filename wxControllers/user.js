const tool = require('../utils/tool')
const logUtil = require('../utils/log_util')
const wxUsermodel = require('../db/models/wxUser')
const WXBizDataCrypt = require('../utils/libs/WXBizDataCrypt')
const _ = require('lodash')
let wx = {
    appId: 'wxed2da0fb9fff4b48',
    appSecret: '57fc45c50f9523cfa3b573764c25a94e'
}
class Controller {
    /**
     * 通过ID返回用户对象
     * @param {*} ctx 
     */
    static async login(ctx) {
        try {
            let [startTime, args] = [new Date().getTime(), ctx.request.body]
            let [encryptedData, iv, userInfo] = [args.encryptedData, args.iv, args.userInfo]

            let {
                code
            } = args
            // 获取session
            let result = await tool.httpGet('https://api.weixin.qq.com/sns/jscode2session', {
                appid: wx.appId,
                secret: wx.appSecret,
                js_code: code,
                grant_type: 'authorization_code'
            })
            // 保存session
            let textObj = JSON.parse(result.text)
            let {
                session_key,
                openid,
                unionid
            } = textObj
            console.log('用户登陆：textObj', JSON.stringify(textObj))

            // if (encryptedData && iv) {
            //     var pc = new WXBizDataCrypt(wx.appId, sessionKey)
            //     var data = pc.decryptData(encryptedData, iv)
            //     console.log('解密后 data: ', data)
            // }

            // 查看用户是否存在
            let one = await wxUsermodel.findOne({
                openid: openid
            })
            if (one) { //存在则更新基础信息
                one.sessionKey = session_key
                if (unionid)
                    one.unionId = unionid
                await one.save();
            } else { //插入新用户
                await (new wxUsermodel({
                    sessionKey: session_key,
                    openid,
                    unionId: unionid
                })).save()
            }
            tool.resSuccess(ctx, textObj)
        } catch (err) {
            tool.resError(ctx, '登陆失败', err)
            logUtil.logInfo('登陆失败' + err);
        }
    }
    /**
     * 新增app类型信息接口
     * @param {*} ctx 
     */
    static async userinfo(ctx) {
        try {
            let [startTime, args] = [new Date().getTime(), ctx.request.body]
            let [encryptedData, iv] = [args.encryptedData, args.iv]

            let {
                userInfo,
                openid
            } = args
            // 查看用户是否存在
            let one = await wxUsermodel.findOne({
                openid
            })
            if (one) { //存在则更新基础信息
                if (encryptedData && iv) {
                    var pc = new WXBizDataCrypt(wx.appId, one.sessionKey)
                    var data = pc.decryptData(encryptedData, iv)
                    console.log('用户信息更新， 解密后 data: ', data)
                    one = Object.assign(one, data)
                    await one.save();
                }
            } else { //插入新用户
                throw '没有该用户'
            }
            tool.resSuccess(ctx, '用户信息更新成功')
        } catch (err) {
            // console.log(err, '')
            tool.resError(ctx, '登陆失败', err)
            logUtil.logInfo('登陆失败' + err);
        }
    }
    /**
     * 新增app类型信息接口
     * @param {*} ctx 
     */
    static async user(ctx) {
        try {
            let [startTime, args] = [new Date().getTime(), ctx.request.body]
            tool.resSuccess(ctx, args)
        } catch (err) {
            tool.resError(ctx, '获取用户失败', err)
            logUtil.logInfo('获取用户失败' + err);
        }
    }

    static async share(ctx) {
        try {
            let [startTime, args] = [new Date().getTime(), ctx.request.body]
            let {
                encryptedData,
                iv,
                openid
            } = args
            let one = await wxUsermodel.findOne({
                openid: openid
            })

            var pc = new WXBizDataCrypt(wx.appId, one.sessionKey)
            var data = pc.decryptData(encryptedData, iv)
            tool.resSuccess(ctx, data)
        } catch (err) {
            tool.resError(ctx, '获取用户失败', err)
            logUtil.logInfo('获取用户失败' + err);
        }
    }

    /**
     * 删除app类型信息接口
     * @param {*} ctx 
     */
    static async delete(ctx) {
        try {
            let args = ctx.request.body;
            let one = await model.findOne(args);
            if (one) one.remove()
            else throw '未找到对应记录'
            tool.resSuccess(ctx, `成功删除`);
        } catch (err) {
            tool.resError(ctx, '删除失败', err)
            logUtil.logInfo('删除错误：' + err)
        }
    }
    /**
     * 修改app类型信息接口
     * @param {*} ctx 
     */
    static async update(ctx) {
        try {
            let args = ctx.request.body;
            let condition = _.pick(args, ['_id'])
            let option = _.omit(args, ['_id'])
            var result = await model.update(condition, option);
            if (result.ok == 0) {
                tool.resError(ctx, '修改出错', '未找到对应记录');
            } else {
                tool.resSuccess(ctx, `修改成功`);
            }
        } catch (err) {
            tool.resError(ctx, '修改出错', err);
            logUtil.logInfo('修改出错' + err);
        }
    }
    /**
     * 获取轮播图
     * @param {*} ctx 
     */
    static async findImgs(ctx) {
        try {
            let result = await model.find({}, {
                name: 1,
                imgUrl: 1,
                toUrl: 1
            })
            tool.resSuccess(ctx, result);
        } catch (err) {
            tool.resError(ctx, '新增失败', err)
            logUtil.logInfo('新增失败' + err)
        }
    }
}

module.exports = Controller;