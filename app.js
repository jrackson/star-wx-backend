const Koa = require('koa')
const app = new Koa()
const views = require('koa-views')
const json = require('koa-json')
const onerror = require('koa-onerror')
const bodyparser = require('koa-bodyparser')
const logUtil = require('./utils/log_util');
const LogDb = require('./controller/LogDbController')

const tool = require('./utils/tool')
const index = require('./routes/index')
const users = require('./routes/users')
const star = require('./routes/star')
const wxUser = require('./routes/wxUser')
const type = require('./routes/type')
const news = require('./routes/news')
const img = require('./routes/img')
const wxapp = require('./routes/wxapp')
const ruzhu = require('./routes/ruzhu')
const message = require('./routes/message')


// error handler
onerror(app)

// middlewares
app.use(bodyparser({
  enableTypes: ['json', 'form', 'text'],
  jsonLimit: '10000kb'
}))

app.use(json({
  pretty: false,
  param: 'pretty'
}))
app.use(require('koa-static')(__dirname + '/dist'))

app.use(views(__dirname + '/views', {
  extension: 'pug'
}))

app.use(async (ctx, next) => {
  let start = new Date()
  try {
    if (ctx.method == 'GET') {
      ctx.request.body = ctx.query
    }
    await next();

    logUtil.logResponse(ctx, new Date() - start);
  } catch (err) {
    ctx.body = tool.resError(ctx, '出错', err);
    // 触发error事件    
    ctx.app.emit('error', err, ctx, new Date() - start);
  }
      LogDb.insert(ctx, new Date() - start)
})


// token
// app.use(require('./token'))
// routes
app.use(index.routes(), index.allowedMethods())
app.use(users.routes(), users.allowedMethods())
app.use(star.routes(), star.allowedMethods())
app.use(wxUser.routes(), wxUser.allowedMethods())
app.use(type.routes(), type.allowedMethods())
app.use(news.routes(), news.allowedMethods())
app.use(img.routes(), img.allowedMethods())
app.use(wxapp.routes(), wxapp.allowedMethods())
app.use(ruzhu.routes(), ruzhu.allowedMethods())
app.use(message.routes(), message.allowedMethods())

// error-handling
app.on('error', (err, ctx, ms) => {
  logUtil.logError(ctx, err, ms);
});

module.exports = app